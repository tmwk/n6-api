<?php

/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 24/03/2017
 * Time: 8:17
 */
namespace Tmwk\N6Bundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class N6Extension extends \Twig_Extension
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('shorten', array($this, 'shorten'), array('is_safe' => array('html'))),
        );
    }

    public function shorten($url, $return = 'url')
    {
        $n6 = $this->container->get('tmwk.n6');

        $shorten = $n6->shorten($url);

        if(isset($shorten->errors)){
            return $this->container->get('twig')->render('@TmwkN6/errors.html.twig', array('errors' => $shorten->errors));
        }

        switch ($return){
            case'url':
                return $shorten->url;
                break;
            case'host':
                return $shorten->host;
                break;
            case'alias':
                return $shorten->alias;
                break;
            case'status':
                return $shorten->status;
                break;
        }

        return $this->container->get('twig')->render('@TmwkN6/errors.html.twig', array('errors' => array('El parametro no es valido.')));
    }
}