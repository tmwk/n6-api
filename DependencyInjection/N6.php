<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 23/03/2017
 * Time: 20:55
 */

namespace Tmwk\N6Bundle\DependencyInjection;


use Curl\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;

class N6
{
    private $curl;
    private $container;
    private $api_token;
    private $host;

    public function __construct(Curl $curl, ContainerInterface $container, $api_token, $host)
    {
        $this->curl = $curl;
        $this->container = $container;
        $this->api_token = $api_token;
        $this->host = $host;
    }

    public function shorten($url)
    {
        $config = [
            'apikey' => $this->api_token,
            'url'    => $url,
        ];
        $this->curl->setHeader('Accept', 'application/json');
        $result = $this->curl->post($this->host.'/api/shorten', $config);

        return $result;
    }

    public function update($url, $current_url)
    {
        $current_url = explode('/', $current_url);

        $config = [
            'apikey' => $this->api_token,
            'alias'  => end($current_url),
            'url'    => $url,
        ];
        $this->curl->setHeader('Accept', 'application/json');
        $result = $this->curl->post($this->host.'/api/shorten', $config);

        return $result;
    }
}